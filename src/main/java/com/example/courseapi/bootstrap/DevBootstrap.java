package com.example.courseapi.bootstrap;

import com.example.courseapi.course.Course;
import com.example.courseapi.course.CourseRepository;
import com.example.courseapi.topic.Topic;
import com.example.courseapi.topic.TopicRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private CourseRepository courseRepository;
    private TopicRepository topicRepository;

    public DevBootstrap(CourseRepository courseRepository, TopicRepository topicRepository) {
        this.courseRepository = courseRepository;
        this.topicRepository = topicRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        Topic javaTopic = new Topic();
        javaTopic.setName("JAVA");
        javaTopic.setDescription("Java Topic Desc");
        topicRepository.save(javaTopic);

        Course javaCourse = new Course();
        javaCourse.setName("Java SpringBoot");
        javaCourse.setDescription("Java Description");
        javaCourse.setTopic(javaTopic);
        courseRepository.save(javaCourse);

        javaCourse = new Course();
        javaCourse.setName("Java FX");
        javaCourse.setDescription("JavaFx Description");
        javaCourse.setTopic(javaTopic);
        courseRepository.save(javaCourse);
    }
}
